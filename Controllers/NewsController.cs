﻿using log4net;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using Dapper;
using System.Data;

namespace CABApi.Controllers {
    [RoutePrefix("api/v1/News")]
    public class NewsController : ApiController {
        public static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        [HttpGet]        
        [Route("list")]
        public List<PlazaNews> List() {
            List<PlazaNews> ncc = null;
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                conn.Open();
                ncc = conn.Query<PlazaNews>(@"
                    select [Sequence], Title, TickerContent, Content, Picture, Publish, [Order], CreatedOn from logiplaza_news where Publish = 1").ToList();
            }
            return ncc;
        }
    }
    public class PlazaNews {

        public int Sequence { get; set; }
        public string Title { get; set; }
        public string TickerContent { get; set; }
        public string Content { get; set; }
        public string Picture { get; set; }
        public int Order { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
