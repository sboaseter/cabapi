﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using Dapper;
using System.Data.SqlClient;

namespace CABApi.Controllers {
    [RoutePrefix("api/v1/Assortiment")]
    public class AssortimentController : ApiController {
        public static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        [HttpPost]
        [Route("list")]
        public List<Assortiment> getAssortiment([FromBody] GrowerChoices choices) {
            List<Assortiment> ncc = null;
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                conn.Open();
                ncc = conn.Query<Assortiment>("[api_getAssortiment] @Grower, @Maingroup, @Language", new { Grower = choices.Sequence, Maingroup = choices.Maingroup, Language = 1 }).ToList();
            }
            return ncc;
        }

    }
    public class Assortiment {
        public int seq { get; set; }
        public int gseq { get; set; }
        public bool active { get; set; }
        public int cabseq { get; set; }
        public string cabcode { get; set; }
        public string prop { get; set; }
        public string desc { get; set; }

        public string Maingroup { get; set; }
        public string Grouptype { get; set; }
        public string Group { get; set; }
        public string Cask { get; set; }

        public int CSEQ { get; set; }
        public decimal CUPL { get; set; }
        public decimal CNOU { get; set; }
        public decimal CLPT { get; set; }
        
        /* Added properties in app */
        public decimal offer_price { get; set; }
        public bool offer_special { get; set; }
        public int offer_units { get; set; }
        public string offer_remark { get; set; }


        public int? cabpictureseq { get; set; }
        public int? graspictureseq { get; set; }
        public int? artpictureseq { get; set; }

        public int? pictureseq {
            get {
                return artpictureseq ?? graspictureseq ?? cabpictureseq;
            }
        }
        public decimal? numitems { get; set; }
        public DateTime? start_date { get; set; }
        public DateTime? end_date { get; set; }

        public string grower_id { get; set; }

        public string PROP_VAL1 { get; set; }
        public string PROP_VAL2 { get; set; }
        public string PROP_VAL3 { get; set; }
        public string PROP_VAL4 { get; set; }
        public string PROP_VAL5 { get; set; }
        public string PROP_VAL6 { get; set; }

        public string PROP_DESC1 { get; set; }
        public string PROP_DESC2 { get; set; }
        public string PROP_DESC3 { get; set; }
        public string PROP_DESC4 { get; set; }
        public string PROP_DESC5 { get; set; }
        public string PROP_DESC6 { get; set; }

        public string Properties {
            get {
                var raw =  String.Format("{0}:{1}, {2}:{3}, {4}:{5}, {6}:{7}, {8}:{9}, {10}:{11}",
                    PROP_DESC1.Length > 1 ? PROP_DESC1.Substring(0, 1) : string.Empty, PROP_VAL1,
                    PROP_DESC2.Length > 1 ? PROP_DESC2.Substring(0, 1) : string.Empty, PROP_VAL2,
                    PROP_DESC3.Length > 1 ? PROP_DESC3.Substring(0, 1) : string.Empty, PROP_VAL3,
                    PROP_DESC4.Length > 1 ? PROP_DESC4.Substring(0, 1) : string.Empty, PROP_VAL4,
                    PROP_DESC5.Length > 1 ? PROP_DESC5.Substring(0, 1) : string.Empty, PROP_VAL5,
                    PROP_DESC6.Length > 1 ? PROP_DESC6.Substring(0, 1) : string.Empty, PROP_VAL6
                    );
                return raw.Replace(":,", string.Empty);
            }
        }

    }
}
