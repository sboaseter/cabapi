﻿using log4net;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using Dapper;
using System.Data;
using System.Net.Mail;
//using System.Web;
using System.IO;
using System.Threading.Tasks;
using System.Text;

namespace CABApi.Controllers {
    [RoutePrefix("api/v1/User")]
    public class UserController : ApiController  {
        public static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        [HttpPost]
        [Route("validate")]
        public LoginInfo Validate([FromBody]ValidateRequest model) {
            LoginInfo li = null;
            
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                li = conn.Query<LoginInfo>("proc_getlogininfo", new { Username = model.Username }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            if (li != null) {
                if (li.LOGI_PASS == model.Password) {
                    Log.InfoFormat("User authenticated: {0}", model.Username);
                    li.Message = "Successfully logged in";
                    li.MessageID = 0;
                    // Fill misc info

                    addLogEntry(li);

                    return li;
                } else {
                    Log.InfoFormat("User entered incorrect credentials: {0}", model.Username);
                    return new LoginInfo {
                        Message = "Incorrect Username or/and Password",
                        MessageID = 1
                    };
                }

            } else {
                Log.Info("Empty LoginInfo object submitted");
                return new LoginInfo {
                    Message = "Username unknown.",
                    MessageID = 2
                };
            }
        }

        [HttpGet]
        [Route("maingroups/{id}")]
        public List<Maingroup> Maingroups(int id) {
            List<Maingroup> mgs = null;
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                conn.Open();
                mgs = conn.Query<Maingroup>(@"
                                            select 
	                        CDMG_SEQUENCE AS [Sequence],
	                        CDMG_DESC as [Description],
	                        CDMG_NUM_OF_DAYS_OFFER as DaysOffer,
	                        ORMA_ORDER AS [Order]
                            from F_CAB_ORGA_MAIN_GROUP_MATRIX 
                        join F_CAB_CD_MAIN_GROUP_TYPE on ORMA_FK_MAIN_GROUP = CDMG_SEQUENCE
                        where orma_fk_orga = @Id", new { Id = id }).ToList();
            }
            return mgs;
        }


 

        private void addLogEntry(LoginInfo li) {
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                conn.Query(@"insert into f_cab_log (calo_fk_login, calo_action, calo_value, calo_created_on) 
                            values (@LoginSequence, 'App', 'Login', @Now)", new { LoginSequence = li.LOGI_SEQUENCE, Now = DateTime.Now });
            }
        }

        [HttpPost]
        [Route("newuser")]
        public bool Newuser([FromBody] NewUser model) {
            if (model != null) {
                Log.InfoFormat("New user requested information from LogiCAB App: {0}", model);
                return true;
                // Perform mailing & inserting into apropriate tables!
            }
            return false;
        }

        [HttpPost]
        [Route("feedback")]
        public string Feedback([FromBody] Feedback fb) {
            LoginInfo li = null;
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                li = conn.Query<LoginInfo>("proc_getlogininfo", new { Username = fb.username}, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            Log.InfoFormat("User sending feedback: {0}", li.ToString());
            Log.InfoFormat("Remarks: {0}", fb.text);
            try {
                Task.Factory.StartNew(() => SendMail(li, fb.text));
                return "Success.";
            } catch (Exception ex) {
                Log.ErrorFormat("Error occured while sending mail: {0}", ex.ToString());
                return "Error.";
            }
            return fb.text;
        }

        private void SendMail(LoginInfo li, string text) {
            SmtpClient SMTP = new SmtpClient();
            SMTP.Host = "smtp.onlinespamfilter.nl";
//            SMTP.Credentials = new System.Net.NetworkCredential("sigurd.boaseter@logiflora.nl", "!@#lf");
            try {
                MailMessage mm = new MailMessage();
                mm.From = new MailAddress("noreply@logiflora.nl");
                mm.To.Add(new MailAddress("support@logiflora.nl"));
                mm.Subject = "LogiCAB App: Feedback from " + li.LOGI_USER_NAME;

                mm.Body = li.ToString() + "\n" + text;

                // While exception exists -> redo SMTP.Send x-times.

                var logroot = System.Web.Hosting.HostingEnvironment.MapPath("~/Logs/feedback");
                bool exists = System.IO.Directory.Exists(logroot);
                if (!exists)
                    System.IO.Directory.CreateDirectory(logroot);

                var mailFile = li.LOGI_USER_NAME + "_" + DateTime.Now.ToLongTimeString().Replace(":", string.Empty) + ".feedback.txt";
                FileStream fs = new FileStream(logroot + "/" + mailFile, FileMode.Append);
                byte[] mailData = Encoding.ASCII.GetBytes(String.Format("From: {0}, To: {1}, Login: {2}, Text: {3}", mm.From.Address, mm.To.FirstOrDefault().Address, li.LOGI_USER_NAME, text));
                fs.Write(mailData, 0, mailData.Length);
                fs.Close();

                SMTP.Send(mm);
            } catch (Exception ex) {
                Log.ErrorFormat("Error occured while sending mail: {0}", ex.ToString());
            }
        }
    }


    public class ValidateRequest {
        public string Username { get; set; }
        public string Password { get; set; }
    }
    public class Feedback {
        public string username { get; set; }
        public string text { get; set; }
    }

    public class LoginInfo {
        public int LOGI_SEQUENCE { get; set; }
        public string LOGI_USER_NAME { get; set; }
        public int LOGI_FK_PERSON { get; set; }
        public string LOGI_PASS { get; set; }
        public int BUYR_SEQUENCE { get; set; }
        public int GROW_SEQUENCE { get; set; }
        public int ORGA_SEQUENCE { get; set; }
        public int? ORGA_FK_PICTURE { get; set; }
        public string PERS_NAME { get; set; }
        public int MAIN_GROUP { get; set; }
        public string Message { get; set; }
        public int MessageID { get; set; }

        public override string ToString() {
            return String.Format("Username: {0}, LogiSeq: {1}, GrowSeq/BuyrSeq: {2}", this.LOGI_USER_NAME, this.LOGI_SEQUENCE, this.BUYR_SEQUENCE != 0 ? this.BUYR_SEQUENCE : this.GROW_SEQUENCE);
        }
    }

    public class NewUser {
        public string name { get; set; }
        public string reference { get; set; }
        public string companyname { get; set; }
        public string address { get; set; }
        public string zipcode { get; set; }
        public string city { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string remarks { get; set; }
        public override string ToString() {
            return String.Format("Name: {0}, Ref: {1}, Company: {2}, Address: {3}, Zipcode: {4}, City: {5},  Email: {6}, Phone: {7}, Remarks: {8}",
                name, reference, companyname, address, zipcode, city, email, phone, remarks);
        }
    }
    public class Maingroup {
        public int Sequence { get; set; }
        public string Description { get; set; }
        public decimal DaysOffer { get; set; }
        public int Order { get; set; }
    }

}
