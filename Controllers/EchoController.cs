﻿using log4net;
using System;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using Dapper;
using System.Data;
using System.IO;
using System.Drawing;
using System.Text;
using System.Web;
using System.Threading.Tasks;

namespace CABApi.Controllers {
    [RoutePrefix("api/v1/Echo")]
    public class EchoController : ApiController {
        public static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        [HttpGet]
        [Route("echo/{data}")]
        public string echo(string data) {
            Log.InfoFormat("EchoService: {0}", data);
            return data;

        }

        [HttpPost]
        [Route("uploadfile")]
        public async Task<IHttpActionResult> uploadFile() {
            Log.Info("Received request to upload image!");
            Log.InfoFormat("Content: {0}", Request.Content);
            if (!Request.Content.IsMimeMultipartContent()) {
                Log.ErrorFormat("Stopped due to unsupported media type: Request.Content.IsFormData {0}", Request.Content.IsFormData());
                Log.ErrorFormat("Stopped due to unsupported media type: Request.Content.IsHttpRequestMessageContent {0}", Request.Content.IsHttpRequestMessageContent());
                Log.ErrorFormat("Request.Content.Headers.ContentType {0}", Request.Content.Headers.ContentType);
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }


            try {
                Log.Info("About to load content into DB.");
            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider);
            foreach (var file in provider.Contents) {
                Log.InfoFormat("Found file: {0}", file);
                var filename = file.Headers.ContentDisposition.FileName.Trim('\"');
                var buffer = await file.ReadAsByteArrayAsync();
                var id = savePictureToDB(buffer);
                return Ok(id);

                //Do whatever you want with filename and its binaray data.
            }
            } catch (Exception ex) {
                Log.ErrorFormat("An error occured: {0}", ex.ToString());
            }

            return Ok();
        }

        private int savePictureToDB(byte[] data) {
            Log.Info("Attempting to store to DB...");
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                conn.Open();

                var p = new DynamicParameters();
                p.Add("Content", data, dbType: DbType.Binary);
                p.Add("Desc", 1);
                p.Add("PICT_SEQUENCE", dbType: DbType.Int32, direction: ParameterDirection.Output);
                conn.Query<int>("api_insertPicture", p, commandType: CommandType.StoredProcedure);
                int id = p.Get<int>("PICT_SEQUENCE");
                Log.InfoFormat("Image saved: {0}", id);
                return id;
            }
        }

        [HttpPost]
        [Route("upload")]
        public string upload([FromBody] ImgUpload model) {
            Log.Info("Received picture...");
            var logroot = HttpContext.Current.Server.MapPath("~/Logs");
            var imgname = DateTime.Now.ToLongTimeString().Replace(":", string.Empty) + ".txt";
            FileStream fs = new FileStream(logroot + "/" + imgname, FileMode.Append);
            byte[] imgdata = Encoding.ASCII.GetBytes(model.data);
            fs.Write(imgdata, 0, imgdata.Length);
            fs.Close();
            if (String.IsNullOrEmpty(model.data)) {
                Log.Info("No data provided, exiting.");
                return string.Empty;
            } else {
                Log.InfoFormat("Data provided, Length: {0}", model.data.Length);
            }

            try {
                var bytes = Convert.FromBase64String(model.data);
                MemoryStream ms = new MemoryStream(bytes);
                Image tempImg = Image.FromStream(ms);

                Log.Info("Attempting to store to DB...");
                using (var conn = new SqlConnection(Config.CabConnectionString)) {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("Content", ms.ToArray(), dbType: DbType.Binary);
                    p.Add("Desc", 1);
                    p.Add("PICT_SEQUENCE", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    conn.Query<int>("api_insertPicture", p, commandType: CommandType.StoredProcedure);
                    int id = p.Get<int>("PICT_SEQUENCE");
                    Log.InfoFormat("Image saved: {0}", id);
                    return id.ToString();
                }
            } catch (FormatException fex) {
                Log.ErrorFormat("Error trying to convert base64string to byte[], error: ", fex.ToString());
            } catch (Exception ex) {
                Log.ErrorFormat("Error occured");
            }
            return string.Empty;
        }

    }

    public class ImgUpload {
        public int seq { get; set; }
        public string data { get; set; }      
    }
}
