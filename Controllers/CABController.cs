﻿using log4net;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using Dapper;
using System.Data;

namespace CABApi.Controllers {
    [RoutePrefix("api/v1/CAB")]
    public class CABController : ApiController {
        public static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        [HttpGet]
        [Route("new")]
        public List<NewCABCode> newCABCodes() {
            List<NewCABCode> ncc = null;
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                conn.Open();
                ncc = conn.Query<NewCABCode>(@"
                    select top 20 orga_name Grower, cabc_cab_Code Code, cabc_cab_desc [Desc], cabc_fk_picture [Picture] from f_Cab_Cd_Cab_Code 
join F_CAB_QUEUE_CODE_REQUEST on qcrq_fk_cab_Code_assigned = cabc_Sequence
join f_Cab_grower on qcrq_fk_Grower = grow_Sequence
join f_Cab_organisation on grow_fk_orga = orga_Sequence
order by cabc_sequence desc").ToList();                
            }
            return ncc;
        }
    }
    public class NewCABCode {
        public string Grower { get; set; }
        public string Code { get; set; }
        public string Desc { get; set; }
        public int Picture { get; set; }
    }
}
