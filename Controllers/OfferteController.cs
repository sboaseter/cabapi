﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dapper;
using System.Reflection;
using log4net;
using System.Data.SqlClient;
using System.Data;

namespace CABApi.Controllers {
    [RoutePrefix("api/v1/Offerte")]
    public class OfferteController : ApiController {
        public static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        [HttpPost]
        [Route("modify")]
        public bool ModifyOfferDetail([FromBody] OfferteRow row) {
            Log.InfoFormat("ModifyOfferDetail: Seq: {0}, New: {1}, Old: {2}", row.OFDT_SEQUENCE, row.OFDT_NUM_OF_ITEMS, row.OFDT_NUM_OF_ITEMS_ORG);
            try {
                using (var conn = new SqlConnection(Config.CabConnectionString)) {
                    var p = new DynamicParameters();
                    p.Add("CABC_SEQ", row.OFDT_FK_CAB_CODE);
                    p.Add("CACA_SEQ", row.OFDT_FK_CACA_SEQ);
                    p.Add("OFHD_NO", row.OFHD_NO);
                    p.Add("GROW_SEQ", row.OFHD_FK_GROWER);
                    p.Add("NUMOFITEMS", row.OFDT_NUM_OF_ITEMS);
                    conn.Query("[api_updatenumberofitems]", p, commandType: CommandType.StoredProcedure);
                }
                return true;
            } catch (Exception ex) {
                Log.ErrorFormat("An error occured while trying to modify offerdetails: {0}", ex.ToString());
                return false;
            }
            return false;
        }

        [HttpPost]
        [Route("list")]
        public List<OfferteHeader> List([FromBody] GrowerChoices choices) {
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                var p = new DynamicParameters();
                p.Add("growerSequence", choices.Sequence);
                p.Add("mainGroupSequence", choices.Maingroup);
                p.Add("groupTypeSequence", -1);
                p.Add("groupSequence", -1);
                p.Add("shopSequence", 1);
                p.Add("showAll", choices.ShowAll);
                return conn.Query<OfferteHeader>("[api_getAllOfferNo]", p, commandType: CommandType.StoredProcedure).ToList();
            }
            return null;
        }

        [HttpPost]
        [Route("listedit")]
        public List<OfferteRow> ListEdit([FromBody] GrowerChoices choices) {
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                var p = new DynamicParameters();
                p.Add("GrowerSequence", choices.Sequence);
                p.Add("BuyerSequence", -1);
                p.Add("Offerno", choices.OfferNo);
                p.Add("MainGroupSequence", choices.Maingroup);
                p.Add("GroupTypeSequence", -1);
                p.Add("GroupSequence", -1);
                return conn.Query<OfferteRow>("api_getOfferDetails", p, commandType: CommandType.StoredProcedure).ToList();
            }
            return null;
        }

        [HttpPost]
        [Route("listoffertebuyers")]
        public List<proc_getOfferHeaderInfo> ListOfferteBuyers([FromBody] GrowerChoices choices) {
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                var p = new DynamicParameters();
                p.Add("OFFER_NO", choices.OfferNo);
                p.Add("GROW_SEQ", choices.Sequence);
                return conn.Query<proc_getOfferHeaderInfo>("[proc_getOfferHeaderInfo]", p, commandType: CommandType.StoredProcedure).ToList();
            }
            return null;
        }

        [HttpPost]
        [Route("listbuyers")]
        public List<OfferteBuyer> ListBuyers([FromBody] SequencePairRequest choices) {
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                var p = new DynamicParameters();
                p.Add("GROW_SEQ", choices.Sequence);
                p.Add("MAINTYPE_SEQ", choices.Maingroup);
                return conn.Query<OfferteBuyer>("api_getBuyerList", p, commandType: CommandType.StoredProcedure).ToList();
            }
            return null;
        }
        
        [HttpPost]
        [Route("offerno")]
        public string OfferNo([FromBody] SequencePairRequest choices) {
            return GetOfferNo(choices.Sequence, false);
        }

        [HttpPost]
        [Route("publish")]
        public string publishOfferte([FromBody] PublishOfferte newOfferte) {
            var success = Save(newOfferte);
            
            // Todo: add mail_yn to PublishOfferte-class, implement recursive mailing to buyers, or one mail?

            return "Success!";
        }

        [HttpPost]
        [Route("delete")]
        public string deleteOfferte([FromBody] GrowerChoices model) {
            try { 
                using (var conn = new SqlConnection(Config.CabConnectionString)) {
                    conn.Open();
                    // Collect sequences in question
                    var seqlist = conn.Query<int>(@"select * from f_cab_offer_headers where ofhd_fk_Grower = @Grower and ofhd_no = @OFHD_NO",
                        new { Grower = model.Sequence, OFHD_NO = model.OfferNo }).ToList();
                    foreach (var s in seqlist) {
                        conn.Query(@"UPDATE F_CAB_OFFER_HEADERS SET OFHD_DELETED_BY_OWNER_YN = 1 WHERE OFHD_SEQUENCE = @Seq", new { Seq = s });
                    }
                    return seqlist.Count + " offers marked as deleted.";
                }
            } catch (Exception ex) {
                var msg = String.Format("An error occured while marking OfferNr: {0} from Grower: {1} as deleted.", model.OfferNo, model.Sequence);
                Log.Error(msg);
                return msg;
            }
        }

        [HttpGet]
        [Route("nextworkingday")]
        public DateTime GetNextWorkingDay() {
            try {
                List<DateTime> sDates = new List<DateTime>();
                using (var conn = new SqlConnection(Config.CabConnectionString)) {
                    sDates = conn.Query<DateTime>(@"select spda_datetime from f_Cab_special_dates").ToList();
                }
                DateTime d = DateTime.Now.AddDays(1);
                while ((d.DayOfWeek == DayOfWeek.Saturday || d.DayOfWeek == DayOfWeek.Sunday) || sDates.Select(x => x.Date).Contains(d.Date))
                    d = d.AddDays(1);
                return d;
            } catch (Exception ex) {
                Log.ErrorFormat("An error occured while attempting to get the next valid workingday: ", ex.ToString());
                return DateTime.Today.AddDays(1);
            }
        }

        public bool Save(PublishOfferte offerte) {

            // Update OFHD_NO systemCounter
            var offernr = GetOfferNo(offerte.Sequence, true);

            try {

                foreach (var b in offerte.Offerte.buyers) {
                    using (var conn = new SqlConnection(Config.CabConnectionString)) {
                        var p = new DynamicParameters();
                        p.Add("GROW_SEQ", offerte.Sequence);
                        p.Add("BUYR_SEQ", b.BuyerSequence);
                        p.Add("CDMG_SEQ", offerte.Maingroup);
                        p.Add("OFHD_NO", offernr);

                        var validFrom = new DateTime(1970, 01, 01).AddMilliseconds(offerte.fromDate).ToLocalTime();
                        validFrom = validFrom.AddSeconds(offerte.fromTime);
                        var validTo = new DateTime(1970, 01, 01).AddMilliseconds(offerte.toDate).ToLocalTime();
                        validTo = validTo.AddSeconds(offerte.toTime);

                        Log.InfoFormat("validFrom: {0}, validTo: {1}", validFrom, validTo);
                        Log.InfoFormat("fromTime: {0}, toTime: {1}", offerte.fromTime, offerte.toTime);


                        p.Add("OFHD_VALID_FROM", validFrom);
                        p.Add("OFHD_VALID_TO", validTo);
                        p.Add("REMARK", string.Empty);
                        p.Add("SHOP_SEQ", 1);
                        p.Add("OfferType", offerte.Offerte.offertype);
                        p.Add("OFHD_SEQ", dbType: DbType.Int32, direction: ParameterDirection.Output);
                        conn.Query<int>("proc_createOfferHeader", p, commandType: CommandType.StoredProcedure);
                        int OfferSequence = p.Get<int>("OFHD_SEQ");
                        Log.InfoFormat("Created new OfferHeader: Seq: {0}, Nr: {1}, Buyer: {2}",
                            OfferSequence, offerte.Offerte.nr, b.BuyerSequence);

                        // Now lets add the details
                        foreach (var det in offerte.Offerte.items) {
                            var d = new DynamicParameters();
                            d.Add("OFDT_FK_OFFER_HEADER", OfferSequence);
                            d.Add("OFDT_FK_CAB_CODE", det.cabseq);
                            d.Add("OFDT_FK_CACA_SEQ", det.CSEQ);
                            d.Add("OFDT_NUM_OF_ITEMS", det.offer_units);
                            d.Add("OFDT_ITEM_PRICE", det.offer_price);
                            d.Add("OFDT_REMARK", det.offer_remark);
                            d.Add("OFDT_FK_PICTURE", det.pictureseq);
                            d.Add("OFDT_SEQ", dbType: DbType.Int32, direction: ParameterDirection.Output);
                            d.Add("OFDT_FK_GROWER_ASSORTIMENT", det.seq);
                            d.Add("OFDT_SPECIAL_YN", det.offer_special);
                            conn.Query<int>("proc_createOfferDetail", d, commandType: CommandType.StoredProcedure);
                            int OfferDetailSequence = d.Get<int>("OFDT_SEQ");
                        }
                    }
                }
                return true;
            } catch (Exception ex) {
                Log.ErrorFormat("An error occured while creating offer: {0}", ex.ToString());
                return false;
            }
            return false;
          
        }




        private string GetOfferNo(int growerSequence, bool updateCounter) {
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                var p = new DynamicParameters();
                p.Add("COUN_TYPE", "OFHD");
                p.Add("GROW_SEQ", growerSequence);
                p.Add("UPDATE_YN", updateCounter ? 1 : 0);
                p.Add("COUN_VALUE", dbType: DbType.String, direction: ParameterDirection.Output, size: 15 );
                conn.Query<string>("proc_getSystemCounter", p, commandType: CommandType.StoredProcedure);
                string offerno = p.Get<string>("COUN_VALUE");
                return offerno;
            }
            return "N/A";
        }


    }

    public class PublishOfferte {
        public int Sequence { get; set; }
        public int Maingroup { get; set; }
        public NewOfferte Offerte { get; set; }
        public long fromDate { get; set; }
        public long toDate { get; set; }

        public int fromTime { get; set; }
        public int toTime { get; set; }
    }

    public class NewOfferte {
        public List<OfferteBuyer> buyers { get; set; }


        public string nr { get; set; }
        public string offertype { get; set; }

        public List<Assortiment> items { get; set; }

    }

    public class GrowerChoices {
        public int Sequence { get; set; }
        public int Maingroup { get; set; }
        public bool ShowAll { get; set; }
        public string OfferNo {get;set;}
    }

    public class SequencePairRequest {
        public int Sequence { get; set; }
        public int Maingroup { get; set; }
    }
    public class OfferteRow { // Extend
        public int OFHD_SEQUENCE { get; set; }
        public string OFHD_NO { get; set; }
        public int OFHD_FK_GROWER { get; set; }
        public int GRAS_SEQUENCE { get; set; }
        public int OFDT_SEQUENCE { get; set; }
        public int OFDT_NUM_OF_ITEMS { get; set; }
        public int OFDT_NUM_OF_ITEMS_ORG { get; set; }
        public int OFDT_FK_CAB_CODE { get; set; }
        public string OFDT_REMARK { get; set; }
        public double OFDT_ITEM_PRICE { get; set; }
        public int CDCA_CASK_CODE { get; set; }
        public string PROP_CAB_DESC { get; set; }
        public int PIC_SEQ { get; set; }
        public int ORDE_NUM_OF_ITEMS { get; set; }
        public int OFDT_FK_CACA_SEQ { get; set; }
        public int IsSpecial { get; set; }

        public int GRAS_NUM_OF_ITEMS { get; set; }

        public int CACA_UNITS_PER_LAYER { get; set; }
        public int CACA_NUM_OF_UNITS { get; set; }
        public int CACA_LAYERS_PER_TROLLEY { get; set; }

        public string PROP_VAL1 { get; set; }
        public string PROP_VAL2 { get; set; }
        public string PROP_VAL3 { get; set; }
        public string PROP_VAL4 { get; set; }
        public string PROP_VAL5 { get; set; }
        public string PROP_VAL6 { get; set; }

        public string PROP_DESC1 { get; set; }
        public string PROP_DESC2 { get; set; }
        public string PROP_DESC3 { get; set; }
        public string PROP_DESC4 { get; set; }
        public string PROP_DESC5 { get; set; }
        public string PROP_DESC6 { get; set; }

        public string Properties {
            get {
                var raw = String.Format("{0}:{1}, {2}:{3}, {4}:{5}, {6}:{7}, {8}:{9}, {10}:{11}",
                    PROP_DESC1.Length > 1 ? PROP_DESC1.Substring(0, 1) : string.Empty, PROP_VAL1,
                    PROP_DESC2.Length > 1 ? PROP_DESC2.Substring(0, 1) : string.Empty, PROP_VAL2,
                    PROP_DESC3.Length > 1 ? PROP_DESC3.Substring(0, 1) : string.Empty, PROP_VAL3,
                    PROP_DESC4.Length > 1 ? PROP_DESC4.Substring(0, 1) : string.Empty, PROP_VAL4,
                    PROP_DESC5.Length > 1 ? PROP_DESC5.Substring(0, 1) : string.Empty, PROP_VAL5,
                    PROP_DESC6.Length > 1 ? PROP_DESC6.Substring(0, 1) : string.Empty, PROP_VAL6
                    );
                return raw.Replace(":,", string.Empty);
            }
        }

    }
    public class OfferteHeader {
        public string OFHD_NO { get; set; }
        public DateTime OFHD_VALID_TO { get; set; }
        public DateTime OFHD_VALID_FROM { get; set; }
        public string OFHD_OFFER_TYPE { get; set; }
    }
    public class OfferteBuyer {
        public int BuyerSequence { get; set; }
        public int OrgaSequence { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Code { get; set; }
        public int? PicSequence { get; set; }
        public string Refer { get; set; }
        public string LastOffer { get; set; }
        public string LastOrder { get; set; }
    }

    public class proc_getOfferHeaderInfo {
        public int OFHD_SEQUENCE { get; set; }
        public int OFHD_FK_GROWER { get; set; }
        public int OFHD_FK_BUYER { get; set; }
        public int OFHD_FK_MAIN_GROUP { get; set; }
        public string OFHD_NO { get; set; }
        public DateTime OFHD_VALID_FROM { get; set; }
        public DateTime OFHD_VALID_TO { get; set; }
        public short OFHD_DELETED_YN { get; set; }
        public string OFHD_REMARK { get; set; }
        public string CDMG_DESC { get; set; }
        public string GROWER_NAME { get; set; }
        public string BUYR_NAME { get; set; }
        public short BUYR_ORDERED { get; set; }
    }
}
