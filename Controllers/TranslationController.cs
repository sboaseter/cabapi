﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dapper;
using CABApi.Models;
using log4net;
using System.Reflection;
using System.Web;
using System.ServiceModel.Channels;

namespace CABApi.Controllers {
    [RoutePrefix("api/v1/Translate")]
    public class TranslationController : ApiController {
        public static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        // Todo: Dictionary for mobile apps here, one time initialize into local storage (max 2mb, hence perfect for dictionary use!)
        [HttpPost]
        [Route("")]
        public TranslationResult Post([FromBody]string label) {
            List<TranslationRecord> _results;
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                _results = conn.Query<TranslationRecord>(@"select CATR_LABEL_NAME as Name, CDLA_ISO as Iso, CATR_LABEL_DESCRIPTION as Translation from F_CAB_TRANSLATION 
                                                        JOIN F_CAB_CD_LANGUAGE on CATR_FK_LANGUAGE = CDLA_SEQUENCE
                                                        WHERE CATR_LABEL_NAME LIKE @Label and CDLA_ISO is not null", new { Label = label }).ToList();
            }
            var retTranslations = _results.Select(x => new string[] { x.Iso, x.Translation }).ToList();
//            Log.InfoFormat("Translation requested from: {2}: [{0}], {1} entries found", label, _results.Count, GetClientIp(Request));
            if (_results.Count == 0) {
                this.RegisterMissingTranslation(1, label);
            }
            return new TranslationResult {
                Label = label,
                Count = _results.Count,
                Translations = retTranslations
            };
        }
        private void RegisterMissingTranslation(int language, string text) {
            
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                conn.Open();
                var entryExists = conn.Query("SELECT CMTR_FK_LANGUAGE, CMTR_LABEL_NAME FROM F_CAB_MISSING_TRANSLATION WHERE CMTR_FK_LANGUAGE = @Language AND CMTR_LABEL_NAME = @Text",
                    new { Language = language, Text = text });
                if (entryExists.Count() == 0) { // Lets insert it
//                    Log.InfoFormat("Adding entry in F_CAB_MISSING_TRANSLATION: Language: {0}, Label: {1}", language, text);
                    var res = conn.Query("INSERT INTO F_CAB_MISSING_TRANSLATION (CMTR_FK_LANGUAGE, CMTR_LABEL_NAME) VALUES (@Language, @Text)", new { Language = language, Text = text });
                }
            }
        }

        private string GetClientIp(HttpRequestMessage request = null) {
            request = request ?? Request;

            if (request.Properties.ContainsKey("MS_HttpContext")) {
                return ((HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.UserHostAddress;
            } else if (request.Properties.ContainsKey(RemoteEndpointMessageProperty.Name)) {
                RemoteEndpointMessageProperty prop = (RemoteEndpointMessageProperty)request.Properties[RemoteEndpointMessageProperty.Name];
                return prop.Address;
            } else if (HttpContext.Current != null) {
                return HttpContext.Current.Request.UserHostAddress;
            } else {
                return null;
            }
        }

    }
}
