﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dapper;
using log4net;
using System.Reflection;
using System.Threading;
using System.ServiceModel.Channels;
using System.Web;

namespace CABApi.Controllers {

    public class WebSettings {
        public string Id { get; set; }
        public string Desc { get; set; }
        public string Val { get; set; }
    }
    public class SequenceLanguageRequest {
        public int Sequence { get; set; }
        public int Language { get; set; }
    }

    

    [RoutePrefix("api/v1/Lookup")]
    public class LookupController : ApiController {
        public static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        [HttpPost]
        [Route("")]
        public List<string> Get() {
            Log.InfoFormat("Received request from: {0}, waiting 1500ms before responding to emulate work", GetClientIp(Request));
            Thread.Sleep(1500);
            return (new string[] { "value1", "value2" }).ToList();
        }
        [HttpGet]
        [Route("testget")]
        public List<string> randomname() {
            Log.InfoFormat("Received request from: {0}, waiting 1500ms before responding to emulate work", GetClientIp(Request));
            Thread.Sleep(1500);
            return (new string[] { "value1", "value2" }).ToList();
        }

        // GET api/values/5
        [HttpGet]
        [Route("{label}")]
        public string Get(string label) {
//            return String.Format("You askes for: {0}", id);
            List<WebSettings> _dbSettings;
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                _dbSettings = conn.Query<WebSettings>(@"
                    SELECT 
                        SETG_CODE_ID Id, 
                        SETG_DESCRIPTION [Desc], 
                        SETG_VALUE Val 
                        FROM F_CAB_SETTINGS 
                    WHERE 
                        SETG_FK_SHOP_SEQ is not null 
                        and SETG_FK_SPSN_SEQ is null 
                        and SETG_FK_ORGA_SEQ is null 
                        and SETG_FK_PERS_SEQ is null").ToList();
            }
            return label;
        }

        // POST api/values
        public void Post([FromBody]string value) {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value) {
        }

        // DELETE api/values/5
        public void Delete(int id) {
        }

        private string GetClientIp(HttpRequestMessage request) {
            if (request.Properties.ContainsKey("MS_HttpContext")) {
                return ((HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.UserHostAddress;
            } else if (request.Properties.ContainsKey(RemoteEndpointMessageProperty.Name)) {
                RemoteEndpointMessageProperty prop;
                prop = (RemoteEndpointMessageProperty)request.Properties[RemoteEndpointMessageProperty.Name];
                return prop.Address;
            } else {
                return null;
            }
        }

    }
}
