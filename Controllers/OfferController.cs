﻿using log4net;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using Dapper;
using System.Data;

namespace CABApi.Controllers {
    [RoutePrefix("api/v1/Offer")]
    public class OfferController : ApiController {
        public static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        [HttpGet]
        [Route("deliverydates/{id}")]
        public List<Deliverydate> GetDeliveryDates(int id) {
            List<Deliverydate> dd = null;
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                dd = conn.Query<Deliverydate>("Proc_GetPossibleDeliveryDates", new { BUYR_SEQ = id }, commandType: CommandType.StoredProcedure).ToList();
            }
            return dd;
        }
        [HttpPost]
        [Route("list")]
        public List<OfferRow> List([FromBody]UserChoices model) {
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                var dd = conn.Query<OfferRow>("api_GetOfferRows", new { Buyer = model.Buyer, SelectedDateTime = model.SelectedDate, Maingroup = model.Maingroup }, commandType: CommandType.StoredProcedure).ToList();
                return dd;
            }
        }

    }

    public class UserChoices {
        public int Buyer { get; set; }
        public int Maingroup { get; set; }
        public DateTime SelectedDate { get; set; }
    }


    public class Deliverydate {
        public int ORGA_SEQ { get; set; }
        public int? OFHD_SEQ { get; set; }
        public int MAIN_GROUP { get; set; }

        public string MaingroupStr {
            get {
                switch (this.MAIN_GROUP) {
                    case 1:
                        return "plants";
                    case 2:
                        return "flowers";
                    case 3:
                        return "fishes";
                    case 4:
                        return "misc";
                    case 5:
                        return "dryflowers";
                    default:
                        return "N/A";
                }
            }
        }
        public bool? Internal { get; set; }
        public string GROWER { get; set; }
        public DateTime VALID_FROM { get; set; }
        public DateTime VALID_TO { get; set; }
        public string OFHD_TYPE { get; set; }
        public DateTime ORDER_DATE { get; set; }
        public DateTime DEL_DATE { get; set; }
        public int Nr_Det { get; set; }
        public int? OFFER_CAPTION { get; set; }
    }

    public class OfferRow { // Extend
        public int S { get; set; } // Sequence
        public int OS { get; set; } // OfferheaderSequence
        public int MG { get; set; } 
        public string CAB { get; set; }
        public int CABS { get; set; }

        public string SN { get; set; }
        public int SPic { get; set; }
        public string SWeb { get; set; }

        public int SP { get; set; } // FK_Grower
        public string GN { get; set; } // Groupname
        public int GS { get; set; } // CABC_FK_CAB_GROUP

        public string GTN { get; set; }
        public int GTS { get; set; } // CDGR_FK_GROUP_TYPE
        public int P { get; set; } // Picture

        public string PictureExt { get; set; }

        public double Price { get; set; }

        public string PCD { get; set; }        

        public string P1 { get; set; }
        public string P2 { get; set; }
        public string P3 { get; set; }
        public string P4 { get; set; }
        public string P5 { get; set; }
        public string P6 { get; set; }

        public string D1 { get; set; }
        public string D2 { get; set; }
        public string D3 { get; set; }
        public string D4 { get; set; }
        public string D5 { get; set; }
        public string D6 { get; set; }

        public string Properties {
            get {
                var raw = String.Format("{0}:{1}, {2}:{3}, {4}:{5}, {6}:{7}, {8}:{9}, {10}:{11}",
                    D1.Length > 1 ? D1.Substring(0, 1) : string.Empty, P1,
                    D2.Length > 1 ? D2.Substring(0, 1) : string.Empty, P2,
                    D3.Length > 1 ? D3.Substring(0, 1) : string.Empty, P3,
                    D4.Length > 1 ? D4.Substring(0, 1) : string.Empty, P4,
                    D5.Length > 1 ? D5.Substring(0, 1) : string.Empty, P5,
                    D6.Length > 1 ? D6.Substring(0, 1) : string.Empty, P6
                    );
                return raw.Replace(":,", string.Empty).Replace(", :", string.Empty);
            }
        }

        public string Cask { get; set; }
        public int UPL { get; set; }
        public int NOU { get; set; }
        public int LPT { get; set; }

        public int GNI { get; set; }
        public int ONI { get; set; } // OFDT_NUM_OF_ITEMS
        public string Remark { get; set; }
        public string ONr { get; set; } // OFHD_NO
        public DateTime VF { get; set; }
        public DateTime VT { get; set; }
        public DateTime LOD { get; set; }
        public DateTime DTE { get; set; } // Latest delivery datetime
        public bool ISP { get; set; } // IsSpecial
        public int IT { get; set; } // In Transit
        public string  C { get; set; } // OFHD_CAPTION
        public string OT { get; set; } // OfferType
    }
}
