﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CABApi.Models;

namespace CABApi.Controllers {
    [RoutePrefix("api/v1/Order")]
    /*
     * OrderController
     * Responsible for:
     * 
     * LogiOfferte App:
     *  Anything to do with viewing orders, changing status, sending EKT/KWB & more.
     *  
     * LogiOffer App:
     *  Anything to do with confirming a shoppingcart & Orderoverview in App.
     * 
     */ 
    public class OrderController : ApiController {
        public static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        [HttpGet]
        [Route("accept/{id}")]
        public bool AcceptOrder(int id) {
            Log.InfoFormat("AcceptOrder Requested for ORHE_SEQUENCE: {0}", id);
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                var p = new DynamicParameters();
                p.Add("ORHE_SEQ", id);
                p.Add("Auto_Confirm", 1);
                p.Add("Confirm_Details", 1);
                conn.Query("proc_updOrderHeader", p, commandType: CommandType.StoredProcedure);
            }

            if (this.handshakeCheckByOrderId(id) != null) {
                Log.InfoFormat("handshakeCheckByOrderId({0}) completed.", id);
            } else {
                Log.InfoFormat("handshakeCheckByOrderId({0}) failed.", id);
            }
            return true;
        }


        [HttpGet]
        [Route("handshakebyorder/{id}")]
        public Handshake handshakeCheckByOrderId(int id) {
            int grower = 0;
            int buyer = 0;
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                var o = conn.Query<Order>(@"SELECT ORHE_FK_GROWER AS Grower, ORHE_FK_BUYER as Buyer from F_CAB_ORDER_HEA WHERE ORHE_SEQUENCE = @Id", new { Id = id }).FirstOrDefault();
                if (o != null) {
                    grower = o.Grower;
                    buyer = o.Buyer;
                } else {
                    return null;
                }
            }
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                var h = conn.Query<Handshake>("[api_getHandshake] @GROW, @BUYR", new { GROW = grower, BUYR = buyer }).FirstOrDefault();
                if (h != null) {
                    this.processHandshake(id, h);
                    return h;
                } else {
                    return null;
                }
            }
            return null;
        }

        private bool processHandshake(int id, Handshake h) {
            try {
                if (h.AutoKWB) {
                    this.SendKWB(id);
                }
                if (h.AutoEKT) {
                    this.SendEKT(id);
                }
                return true;
            } catch (Exception ex) {
                Log.ErrorFormat("An error occured while sending EKT/KWB: {0}", ex.ToString());
                return false;
            }
        }

        [HttpPost]
        [Route("modify")]
        public List<OrderDetail> Modify([FromBody] OrderModified model) {
            Log.InfoFormat("Modifying order: {0}", model.Seq);
            foreach (var d in model.Details) {
                Log.InfoFormat("\t{0}: Units: {1}, Price: {2}", d.Seq, d.Units, d.Price);
            }
            // Modify details, and modify orderheader status

            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                conn.Open();
                
                foreach (var d in model.Details) {
                    conn.Query(@"UPDATE F_CAB_ORDER_DET SET 
                                ORDE_FK_ORDER_DET_STATUS = 5,
                                ORDE_NUM_OF_UNITS = @Units,
                                ORDE_ITEM_PRICE = @Price,
                                ORDE_NUM_OF_ITEMS = @Units * ORDE_NUM_OF_ITEMS_PER_UNIT,
                                ORDE_NETT_LINE_VALUE = @Price * @Units * ORDE_NUM_OF_ITEMS_PER_UNIT
                                WHERE ORDE_SEQUENCE = @Seq", new { Seq = d.Seq, Units = d.Units, Price = d.Price });

                }
                this.confirmOrder(new Order { Sequence = model.Seq });
                conn.Query(@"UPDATE F_CAB_ORDER_HEA SET ORHE_FK_ORDER_STATUS = 9 WHERE ORHE_SEQUENCE = @Seq", new { Seq = model.Seq });
            }

            return Listorderdetails(model.Seq);
        }

        [HttpPost]
        [Route("list")]
        public List<OrderHeader> Listorders([FromBody]SequenceDateRequest model) { // Sequence and fromDate
            var ret = getOrderHeaders(model);
            return ret;
        }

        [HttpGet]
        [Route("details/{id}")]
        public List<OrderDetail> Listorderdetails(int id) {
            List<OrderDetail> ncc = null;
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                conn.Open();
                ncc = conn.Query<OrderDetail>("[api_getOrderDetailInfo] @ORHE_SEQ", new { ORHE_SEQ = id }).ToList();
            }
            return ncc;
        }

        [HttpGet]
        [Route("sendekt/{id}")]
        public bool SendEKT(int id) {
            Log.InfoFormat("EKT Requested for ORHE_SEQUENCE: {0}", id);
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(Config.LogiCABUrl);
                    var content = new FormUrlEncodedContent(new[] { new KeyValuePair<string, string>("", id.ToString()) });
                    var result = client.PostAsync("api/OrderAPI/SendEKT", content).Result;
                    string resultContent = result.Content.ReadAsStringAsync().Result;
                }
            } catch (Exception ex) {
                Log.ErrorFormat("Error occured while trying to send EKT: OrderHeader: [{0}]. Error: {1}", id, ex.ToString());
            }
            return true;
        }

        [HttpGet]
        [Route("sendkwb/{id}")]
        public bool SendKWB(int id) {
            Log.InfoFormat("KWB Requested for ORHE_SEQUENCE: {0}", id);
            try {
                using (var client = new HttpClient()) {
                    client.BaseAddress = new Uri(Config.LogiCABUrl);
                    var content = new FormUrlEncodedContent(new[] { new KeyValuePair<string, string>("", id.ToString()) });
                    var result = client.PostAsync("api/OrderAPI/SendKWB", content).Result;
                    string resultContent = result.Content.ReadAsStringAsync().Result;
                }
            } catch (Exception ex) {
                Log.ErrorFormat("Error occured while trying to send KWB: OrderHeader: [{0}]. Error: {1}", id, ex.ToString());
            }
            return true;
        }

        private List<OrderHeader> getOrderHeaders(SequenceDateRequest model) {
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                conn.Open();
                if (model.Grower > 0) { 
                    var res = conn.Query<OrderHeader>(@"
                        select 
						    orhe_desc [Desc],
	                        orhe_sequence Seq, 
	                        orhe_no Name, 
	                        orhe_created_on CreatedDate, 
	                        orhe_date OrderDate, 
	                        ORHE_DELIVERY_DATE DeliveryDate,
	                        ORHE_NETT_HEADER_VALUE Value,
	                        orga_name Orga,
						    isnull(orga_fk_picture, -1) Logo,
                            ORGA_SEQUENCE OrgaSeq,
	                        orhe_remark Remark,
	                        ORHE_FK_ORDER_STATUS [Status],
	                        ORHE_FK_BULK_CASK [Bulk],
                            CDCA_CASK_DESC

                        from f_cab_order_hea
                        join f_cab_buyer on orhe_fk_buyer = buyr_Sequence
                        join F_CAB_ORGANISATION on buyr_fk_orga = orga_sequence
                        join F_CAB_CD_CASK ON ORHE_FK_BULK_CASK = CDCA_SEQUENCE
                        where 
	                        ORHE_FK_GROWER = @Grower
                            and ORHE_CREATED_ON > @FromDate
    --                    and ORHE_FK_ORDER_STATUS in (2,10,4,5,14) -- confirmed, ordered, kwbsent, ektkwbsent, ektsent
                        and ORHE_FK_ORDER_STATUS <> 6
                        order by orhe_sequence desc, ORHE_FK_ORDER_STATUS desc", new { Grower = model.Grower, FromDate = model.FromDate });
                    return res.ToList();
                } else {
                    var res = conn.Query<OrderHeader>(@"
                        select 
						    orhe_desc [Desc],
	                        orhe_sequence Seq, 
	                        orhe_no Name, 
	                        orhe_created_on CreatedDate, 
	                        orhe_date OrderDate, 
	                        ORHE_DELIVERY_DATE DeliveryDate,
	                        ORHE_NETT_HEADER_VALUE Value,
	                        orga_name Orga,
						    isnull(orga_fk_picture, -1) Logo,
                            ORGA_SEQUENCE OrgaSeq,
	                        orhe_remark Remark,
	                        ORHE_FK_ORDER_STATUS [Status],
	                        ORHE_FK_BULK_CASK [Bulk],
                            CDCA_CASK_DESC

                        from f_cab_order_hea
                        --join f_cab_buyer on orhe_fk_buyer = buyr_Sequence
                        join f_cab_grower on orhe_fk_grower = grow_Sequence
                        join F_CAB_ORGANISATION on grow_fk_orga = orga_sequence
                        join F_CAB_CD_CASK ON ORHE_FK_BULK_CASK = CDCA_SEQUENCE
                        where 
	                        ORHE_FK_BUYER = @Buyer
                            and ORHE_CREATED_ON > @FromDate
    --                    and ORHE_FK_ORDER_STATUS in (2,10,4,5,14) -- confirmed, ordered, kwbsent, ektkwbsent, ektsent
                        and ORHE_FK_ORDER_STATUS <> 6
                        order by orhe_sequence desc, ORHE_FK_ORDER_STATUS desc", new { Buyer = model.Buyer, FromDate = model.FromDate });
                    return res.ToList();
                }
            }
        }

        #region ConfirmOrder
        /* Buyer-section */
        [HttpPost]
        [Route("confirm")]
        public Order ConfirmOrder([FromBody]Order model) {
            try {
                Log.InfoFormat("ConfirmOrder Requested. Grower: {0}, Buyer: {1}, SelectedDate: {2}", model.Grower, model.Buyer, model.SelectedDate);
                var newOrderHeader = createOrderHeader(model);
                model.Sequence = newOrderHeader;
                Log.InfoFormat("ConfirmOrder: New OrderHeader created: ORHE_SEQUENCE: {0}", model.Sequence);
                for(int i=0;i<model.Items.Count;i++) {
                    model.Items[i].Orderdetail = createOrderDetail(model.Items[i], model.Sequence);
                }
                for (int i = 0; i < model.Items.Count; i++) {
                    if (!String.IsNullOrEmpty(model.Items[i].Custcode)) { // Custcode contains  something, add it
                        using (var conn = new SqlConnection(Config.CabConnectionString)) {
                            conn.Query("UPDATE F_CAB_ORDER_DET SET ORDE_CUSTOMER_CODE = @Custcode, ORDE_STICKER_YN = @Sticker  WHERE ORDE_SEQUENCE = @Sequence", new { Custcode = model.Items[i].Custcode, Sticker = model.Items[i].Sticker, Sequence = model.Items[i].Orderdetail });
                        }
                    }

                }
                Log.InfoFormat("ConfirmOrder: {0} OrderDetails created for OrderHeader[{1}]", model.Items.Count, model.Sequence);
                // Orderheader and Orderdetails created
                confirmOrder(model);
                return model;
            } catch (Exception ex) {
                Log.ErrorFormat("Unforseen error occured while confirming order: {0}, error: {1}", model.Sequence, ex.ToString());
            }
            return null;
        }

        private void confirmOrder(Order order) {
            // Fetch grower/buyer and determine handshake options
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                var p = new DynamicParameters();
                p.Add("ORHE_SEQ", order.Sequence);
                p.Add("Confirm_Details", 1);
                conn.Query("proc_updOrderHeader", p, commandType: CommandType.StoredProcedure);
            }
            if (this.handshakeCheckByOrderId(order.Sequence) != null) {
                Log.InfoFormat("handshakeCheckByOrderId({0}) completed.", order.Sequence);
            } else {
                Log.InfoFormat("handshakeCheckByOrderId({0}) failed.", order.Sequence);
            }
        }

        private int createOrderDetail(OrderItem item, int header) {
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                var p = new DynamicParameters();
                p.Add("OFDT_SEQ", item.Sequence);
                p.Add("ORHE_SEQ", header);
                p.Add("NUM_OF_UNITS", item.Numitems);
                p.Add("SHOP_SEQ", 1);
                p.Add("ORDE_SEQ", dbType: DbType.Int32, direction: ParameterDirection.Output);
                p.Add("APE", null);
                conn.Query<int>("proc_createOrderDetail", p, commandType: CommandType.StoredProcedure);
                int newOrderdetail = p.Get<int>("ORDE_SEQ");
                return newOrderdetail;
            }
        }

        private int createOrderHeader(Order order) {
            using (var conn = new SqlConnection(Config.CabConnectionString)) {
                var p = new DynamicParameters();
                p.Add("ORHE_DESC", string.Empty);
                p.Add("ORHE_DATE", order.SelectedDate);
                p.Add("BUYR_SEQ", order.Buyer);
                p.Add("GROW_SEQ", order.Grower);
                p.Add("CDMG_SEQ", order.Maingroup);
                p.Add("CASK_SEQ", null);
                p.Add("PERS_SEQ", order.Person);
                p.Add("REMARK", string.Empty);
                p.Add("STATUS", "NEW");
                p.Add("SHOP_SEQ", 1);
                p.Add("ORHE_SEQ", dbType: DbType.Int32, direction: ParameterDirection.Output);
                p.Add("OrderType", order.Offertype);
                p.Add("ORHE_DELIVERY_DATE", order.SelectedDate);
                conn.Query<int>("proc_createOrderHeader", p, commandType: CommandType.StoredProcedure);
                int newOrderheader = p.Get<int>("ORHE_SEQ");
                return newOrderheader;
            }
        }
        #endregion
    }
    public class SequenceDateRequest {
        public int Grower { get; set; }
        public int Buyer { get; set; }
        public DateTime FromDate { get; set; }
    }

    public class OrderModified {
        public int Seq { get; set; }
        public List<OrderDetailModified> Details { get; set; }
    }

    public class OrderDetailModified {
        public int Seq { get; set; }
        public int Units { get; set; }
        public decimal Price { get; set; }
    }

    public class Order {
        public int Sequence { get; set; }
        public int Buyer { get; set; }
        public int Grower { get; set; }
        public int Person { get; set; }
        public string Offertype { get; set; }
        public int Maingroup { get; set; }
        public DateTime SelectedDate { get; set; }
        public List<OrderItem> Items { get; set; }
    }
    public class OrderItem {
        public int Sequence { get; set; }
        public int Orderdetail { get; set; }
        public int Numitems { get; set; }
        public int Status { get; set; }
        public bool Sticker { get; set; }
        public string Custcode { get; set; }
    }
    public class OrderHeader {
        public int Seq { get; set; }
        public int Logo { get; set; }
        public string Name { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime DeliveryDate { get; set; }
        public decimal Value { get; set; }
        public string Orga { get; set; }
        public int OrgaSeq { get; set; }
        public string Remark { get; set; }
        public int Status { get; set; }
        public int Bulk { get; set; }
        public string CDCA_CASK_DESC { get; set; }
        public string Desc { get; set; }
        public int? TransporterOrga { get; set; }
    }
    public class OrderDetail {
        
        public int ORDE_SEQUENCE { get; set; }
        public int ORDE_NUM_OF_ITEMS { get; set; }
        public int ORDE_NUM_OF_UNITS { get; set; }
        public decimal ORDE_ITEM_PRICE { get; set; }
        public decimal ORDE_NETT_LINE_VALUE { get; set; }
        public string ORDE_CUSTOMER_CODE { get; set; }
        public short ORDE_STICKER_YN { get; set; }
        public string DESC_PROPS { get; set; }
        public string CAB_DESC { get; set; }
        public int PIC_SEQ { get; set; }
        public string CDCA_CASK_CODE { get; set; }
        public int GRAS_NUM_OF_ITEMS { get; set; }

        public int CACA_NUM_OF_UNITS { get; set; }

        public int Status { get; set; }

        public string PROP_VAL1 { get; set; }
        public string PROP_VAL2 { get; set; }
        public string PROP_VAL3 { get; set; }
        public string PROP_VAL4 { get; set; }
        public string PROP_VAL5 { get; set; }
        public string PROP_VAL6 { get; set; }

        public string PROP_DESC1 { get; set; }
        public string PROP_DESC2 { get; set; }
        public string PROP_DESC3 { get; set; }
        public string PROP_DESC4 { get; set; }
        public string PROP_DESC5 { get; set; }
        public string PROP_DESC6 { get; set; }

        public string Properties {
            get {
                var raw = String.Format("{0}:{1}, {2}:{3}, {4}:{5}, {6}:{7}, {8}:{9}, {10}:{11}",
                    PROP_DESC1.Length > 1 ? PROP_DESC1.Substring(0, 1) : string.Empty, PROP_VAL1,
                    PROP_DESC2.Length > 1 ? PROP_DESC2.Substring(0, 1) : string.Empty, PROP_VAL2,
                    PROP_DESC3.Length > 1 ? PROP_DESC3.Substring(0, 1) : string.Empty, PROP_VAL3,
                    PROP_DESC4.Length > 1 ? PROP_DESC4.Substring(0, 1) : string.Empty, PROP_VAL4,
                    PROP_DESC5.Length > 1 ? PROP_DESC5.Substring(0, 1) : string.Empty, PROP_VAL5,
                    PROP_DESC6.Length > 1 ? PROP_DESC6.Substring(0, 1) : string.Empty, PROP_VAL6
                    );
                return raw.Replace(":,", string.Empty);
            }
        }

    }
    
}
