﻿using System;
using System.Configuration;

namespace CABApi {
    public static class Config {
        private static string _cabConnectionString;
        public static string CabConnectionString {
            get {
                if (!string.IsNullOrEmpty(_cabConnectionString)) return _cabConnectionString;
                ConnectionStringSettings _cabConn = ConfigurationManager.ConnectionStrings["CABConnection"];
                if (_cabConn == null || string.IsNullOrEmpty(_cabConn.ConnectionString))
                    throw new Exception("Fatal error: missin CABConnection string in web.config file");
                _cabConnectionString = _cabConn.ConnectionString;
                return _cabConnectionString;
            }
        }
        private static string _LogiCABUrl;
        public static string LogiCABUrl {
            get {
                if (!string.IsNullOrEmpty(_LogiCABUrl)) return _LogiCABUrl;
                string lcu = ConfigurationManager.AppSettings["LogiCABUrl"];
                _LogiCABUrl = lcu;
                return _LogiCABUrl;
            }
        }
    }
}
 