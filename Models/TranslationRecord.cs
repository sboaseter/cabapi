﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CABApi.Models {
    public class TranslationRecord {
//        public string Name { get; set; }
        public string Iso { get; set; }
        public string Translation { get; set; }
    }
}