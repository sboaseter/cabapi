﻿using System.Collections.Generic;

namespace CABApi.Models {
    public class TranslationResult {
        public string Label { get; set; }
        public int Count { get; set; }
        public List<string[]> Translations { get; set; }
    }
}