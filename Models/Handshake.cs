﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CABApi.Models {
    public class Handshake {
        public int Id { get; set; }
        public string Grower{ get; set; }
        public string Buyer { get; set; }
        public string RelationType { get; set; }

        public bool AutoConfirm { get; set; }
        public bool AutoEKT { get; set; }
        public bool AutoKWB { get; set; }

        public bool SendConfirmMail { get; set; }
        public bool ReceiveOfferMail { get; set; }
        public bool AutoPublishStock { get; set; }
        public bool AutoPublishOffer { get; set; }
        public DateTime LatestDeliveryTime { get; set; }
        public string DeliveryType { get; set; }

        public bool DeliveryStockAllWorkdays { get; set; }
        public bool DeliveryOfferAllWorkdays { get; set; }

    }
}